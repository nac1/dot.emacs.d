;;; -*- no-byte-compile: t -*-
(define-package "flymake-eslint" "20191129.1558" "A Flymake backend for Javascript using eslint" '((emacs "26.0")) :commit "6e2d376f84ddf9af593072954c97e9c82ab85331" :authors '(("Dan Orzechowski")) :maintainer '("Dan Orzechowski") :url "https://github.com/orzechowskid/flymake-eslint")
