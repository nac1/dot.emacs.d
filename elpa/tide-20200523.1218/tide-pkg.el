(define-package "tide" "20200523.1218" "Typescript Interactive Development Environment"
  '((dash "2.10.0")
    (s "1.11.0")
    (flycheck "27")
    (typescript-mode "0.1")
    (cl-lib "0.5"))
  :commit "bdfb7fefdafe96a689a5c3da73fb82c7932a466b" :keywords
  '("typescript")
  :authors
  '(("Anantha kumaran" . "ananthakumaran@gmail.com"))
  :maintainer
  '("Anantha kumaran" . "ananthakumaran@gmail.com")
  :url "http://github.com/ananthakumaran/tide")
;; Local Variables:
;; no-byte-compile: t
;; End:
